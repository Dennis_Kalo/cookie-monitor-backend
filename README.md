# cookie-monitor-backend

## Description ##
This repo hosts the backend for the cookie monitor extention.


## Prerequisites: ##
- NodeJS & npm:<br> 
```https://www.npmjs.com/get-npm ```
- http-server: <br>
```npm install -g http-server```

## Installation ##
1. <b> Download the repo </b>

        $ git clone https://gitlab.com/Dennis_Kalo/cookie-monitor-backend
        $ cd cookie-monitor-backend/

2. <b> Install dependencies </b>
    
        $ npm install

3. <b> Start the server </b> 

        $ npm run start