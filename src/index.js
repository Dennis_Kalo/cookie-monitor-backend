const express = require('express');
const app = express();
const routes = require('./router');
const bodyParser = require('body-parser')
const port = 3000;

app.use(bodyParser.json())
app.use(
    bodyParser.urlencoded({
        extended: true,
    })
)
app.use('/', routes);

app.listen(port, () => {
    console.log('Cookie monitor API running on port: ' + port + '.');
})