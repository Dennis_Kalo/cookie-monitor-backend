'use strict';
var express = require('express');
var router = express.Router();
const domainLookup = require('./whois');


router.post('/whois', domainLookup);

// other requests return error
router.all('*', function (req, res) {
    res.status(404).send('Incorrect request.');
})

module.exports = router;