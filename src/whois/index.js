'use strict';

const findRegistrant = require('./lookup');

async function domainLookup(request, response) {
    const { domain = null} = request.body;

    console.log('Performing whois for domain: ', domain);
    if(!domain){
        console.log('Unable to find Registrant information');
        response.status(400).send('domain is not specified');
    }
    const vendor = await findRegistrant(domain);
    console.log('Registrant Orgranization: ', vendor);
    response.status(200).send(vendor);
}

module.exports = domainLookup;